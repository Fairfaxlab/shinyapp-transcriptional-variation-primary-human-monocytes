# README #

This web page accompanies the manuscript titled 'Genetic Determinants of Transcriptional Variation in Primary Human Monocytes Across Multiple Contexts', by Fairfax et al, which has been published in ---. If you want to use any of the cis-eQTL results displayed on this page in your publication, please cite this paper and related puplication. For further questions, contact the corresponding author: benjamin.fairfax@oncology.ox.ac.uk.

### What is this repository for? ###

We have made this Shiny application available for all lead cis-eQTL, tQTL, and isoQTLs in naïve and stimulated primary human monocyte cells.

### How do I get set up? ###

First use an R integrated development environment (IDE) and install all realted libraries listed in "Dependencies- R-Package.txt" file.

Nest, run the "RUN-ME.R" file.
 